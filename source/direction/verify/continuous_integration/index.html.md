---
layout: markdown_page
title: "Category Vision - Continuous Integration"
---

- TOC
{:toc}

## Continuous Integration

Continuous Integration is an important part of any software development pipeline. It must be easy to use, reliable, and accurate in terms of results, so that's the core of where we focus for CI. While we are very proud that GitLab CI/CD is recognized as [the leading CI/CD tool on the market](/blog/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/), as well as a leader in the 2019 Q3 [Cloud Native CI Wave](/resources/forrester-wave-cloudnative-ci/), it's important for us that we continue to innovate in this area and provide not just a "good enough" solution, but a great one.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Integration)
- [Overall Vision](/direction/cicd#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1304) where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

There are a couple related areas you may be looking for:

- Because CI is such a broad category, a lot of the [CI/CD vision and themes](https://about.gitlab.com/direction/cicd/) are directly applicable here. Since this page is a subset of that one we don't duplicate the content here, but it's also worth reading if you're interested in how we're thinking about CI/CD at GitLab especially over the medium to long term.
- The GitLab Runner is managed by the Verify team also, and is a key component of the CI solution. It has its own strategy that can be found on its [direction page](/direction/verify/runner/)
- Continuous Delivery (and Progressive Delivery) is an important topic that is intimately bound to CI. You can read more about our strategy for CD at our [direction page](/direction/release/continuous_delivery/), and more broadly about our strategy for releasing software at our [Release stage direction page](/direction/release/).
- Our fulfillment team is responsible for allocation of our shared runner fleet, and is looking at how we can bring shared runners to self-managed instances ([gitlab-org&835](bringing GitLab Runners to self-managed (https://gitlab.com/groups/gitlab-org/-/epics/835)))

## What's Next & Why

Our single next most important item is adding a control to your projects, ensuring only the appropriate team members are able to edit the `.gitlab-ci.yml` ([gitlab#15632](https://gitlab.com/gitlab-org/gitlab/issues/15632)). This will help teams ensure control over who is able to implement changes to the build and release process, an important consideration for larger organizations. This is being built as a special case of [gitlagb#14376](https://gitlab.com/gitlab-org/gitlab/issues/14376), which implements the ability to host the `.gitlab-ci.yml` outside of the repository, so in this way we're getting two great features at once.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)), and it is important to us that we defend that position in the market. As such, we are balancing prioritization of [important P2 issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=P2) and [items from our backlog of popular smaller feature requests](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Acontinuous+integration&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93) in addition to delivering new features that move the vision forward. If you feel there are any gaps or items that would risk making GitLab no longer lovable for you, please let us know by contacting [Jason Lenny](https://gitlab.com/jlenny) (PM for this area) with your ideas.

### Monorepos and Cross-Project Pipelines

One major area of focus and expansion is around these two important types of pipeline use-cases. Monorepos are where you put all of your projects into a single repository, and cross-project piplelines (taken to an extreme) are where you might put every service in a microservice architecture into different repositories. There are pros and cons to each approach, but we want to be sure GitLab effectively supports both.

- In support of both, we recently introduced the Directed Acyclic Graph. You can read more about the follow-ups we have planned in the epic [gitlab-org&1716](https://gitlab.com/groups/gitlab-org/-/epics/1716). 
- The next most important issue is [child/parent pipelines](https://gitlab.com/gitlab-org/gitlab/issues/16094), which introduces the ability to trigger child pipelines within the same project, in just the same way you can across projects.

## Competitive Landscape

To be more competitive we are considering [gitlab#10107](https://gitlab.com/gitlab-org/gitlab/issues/10107) to move GitLab CI/CD for External Repos (GitHub + BitBucket) from Premium to Starter. This will help more teams have access to GitLab CI/CD, and is potentially a great way to introduce more people to GitLab in general, but also comes with costs of running the CI/CD so the decision needs to be weighed closely against the opportunity.

### Microsoft

The most urgent gap that we want to address in competing with Microsoft is the capability to have Windows unners as part of our shared fleet.  This is technically part of our [runner category vision](/direction/verify/runner/) but is a critical unlock for CI in general.

#### GitHub CI/CD

GitHub is evolving Actions into more and more of a standalone CI/CD solution. GitLab is far ahead in a lot of areas of CI/CD that they are going to have to catch up on, but Microsoft and GitHub have a lot of resources and have a big user base ready and excited to use their free product. Making an enterprise-ready solution will take some time, but they are no doubt actively working on closing these gaps.

Actions itself is still early days. At first blush it's nice that there are small, individually contributed pluggable modules that can do different things. There's some question, though, as to how this will scale over the medium term. The semi-distributed, event-based interaction model poses some risk of emergent pipeline behaviors becoming difficult for a human to understand, but does provide some nice flexibility in terms of relating how different events in the system can trigger different responses. Similarly, it is yet to be seen how they will avoid the Jenkins issue where many different behaviors (plugins as analog to actions) will be maintained over time (or not) by the enterprises who depend on them, and how these will be compatible with each other as complexity increases over time. There are also potential security concerns with maliciously controlled actions running code as part of your pipelines; in ecosystems where using lots of small packages from different authors is normalized (i.e., NPM) auditing and scanning to mitigate this can become burdensome.

They are planning to bundle in secrets management, and we are also exploring this in our [Secrets Management category](/direction/release/secrets_management). We also have a [Package Management category](/direction/package/package_management) which is analogous to GitHub Packages. Continuous Delivery elements can be found in our [Continuous Delivery category](/direction/release/continuous_delivery).

#### Azure DevOps

Microsoft has also provided a [feature comparison between Azure DevOps (though not GitHub CI/CD) and GitLab](https://docs.microsoft.com/en-us/azure/devops/learn/compare/azure-devops-vs-gitlab) which, although it is not 100% accurate, highlights the areas where they see an advantage over us. Our responses overall as well as to the inaccuracies can be found in [our comparison page](azure_devops_detail.html).

### CloudBees Jenkins/CodeShip

Jenkins has become an industry punching bag for selling DevOps software (just saying "my product solves the same problems as Jenkins, but without the huge expensive legacy mess" gives you the keys to the kingdom.) Jenkins is trying to address this by [splitting](https://jenkins.io/blog/2018/08/31/shifting-gears/) their product into multiple offerings, giving them the freedom to shed some technical and product debt and try to innovate again. They also acquired CodeShip, a SaaS solution for CI/CD so that they can play in the SaaS space. All of this creates a complicated message that doesn't seem to be resonating with analysts or customers yet.

### Bitbucket Pipelines and Pipes
BitBucket pipelines has been Atlassian's answer to a more CI/CD approach than the traditional Atlassian Bamboo that is very UI driven builds and deploys by allowing users to create yml based builds. On February 28, 2019, Atlassian announced [Bitbucket Pipes](https://bitbucket.org/blog/meet-bitbucket-pipes-30-ways-to-automate-your-ci-cd-pipeline) as an evolution of Bitbucket pipelines and gained a lot of press around [Atlassian taking on GitHub/Microsoft](https://www.businessinsider.com/atlassian-bitbucket-pipes-ci-cd-gitlab-github-2019-2). While it is early in the evolution of this as a product in the enterprise market, there are a number of interesting patterns in the announcement that favor Convention over Configuration.

### CodeFresh

CodeFresh follows a similar [container-based plugin model](https://steps.codefresh.io/) as GitHub Actions, so our approach is similar to the one written above in response to that solution.

## Analyst Landscape

There are a few key findings from the Forrester Research analysts on our CI solution:

- In terms of Cloud Native CI, GitLab's solution is seen as capable as the solutions provided by the hyperclouds themselves, and well ahead of other neutral solutions. This can give our users flexibility when it comes to which cloud provider(s) they want to use.
- GitLab is seen as the best end to end leader where other products are not keeping up and not providing a comprehensive solution. We should continue to build a deep solution here in order to stay ahead of competitor's solutions.
- Competitors claim that GitLab can go down and doesn't scale, which is perhaps their best argument if their products do not provide as comprehensive solutions. This isn't really true, so we need to have messaging and product features that make this clear.

To continue to drive innovation in this area, we are considering [gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306) next to add Vault integration throughout the CI pipeline. This builds off of work happening on the Configure team and will allow for a more mature delivery approach that takes advantage of ephemeral credentials to ensure a rock solid secure pipeline.

## Top Customer Success/Sales Issue(s)

The most popular Customer Success issues as determined in FQ1-20 survey of the Technical Account Managers were:

* [`include:` keyword not expanding variables](https://gitlab.com/gitlab-org/gitlab/issues/24811)
* [Filter pipelines by status or branch](https://gitlab.com/gitlab-org/gitlab/issues/15268)

## Top Customer Issue(s)

Our most popular customer issue is to add an option to keep only the latest artifact in each branch ([gitlab#16267](https://gitlab.com/gitlab-org/gitlab/issues/16267)), followed by child parent pipelines ([gitlab#16094](https://gitlab.com/gitlab-org/gitlab/issues/16094)), and then cross-project artifact dependencies ([gitlab#14311](https://gitlab.com/gitlab-org/gitlab/issues/14311)). All of these are scheduled for upcoming releases.

Other items with a lot of intention include normalizing job tokens in a more flexible way, so that they can have powerful abilities when needed and still not introduce security risks ([gitlab#20416](https://gitlab.com/gitlab-org/gitlab/issues/20416)), 

We also have a few issues about making variables avaiable before includes are processed, however there is a "chicken and egg" problem here that has been difficult to solve. Child/parent pipelines solves some use cases, but not all, and in the meantime we are continuing the discussion in issues like [gitlab#24811](https://gitlab.com/gitlab-org/gitlab/issues/24811) and [gitlab#1809](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809). If you're interested in technical discussion around the challenges and want to participate in solving them, please see the conversation [here](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809#note_225636231).

## Top Internal Customer Issue(s)

Supporting CI configuration definition outside of the repository is currently our top internal customer issue: [gitlab#14376](https://gitlab.com/gitlab-org/gitlab/issues/14376).  Other top internal customer issues include:

* Showing a high visiblity alert if master is red ([gitlab#10216](https://gitlab.com/gitlab-org/gitlab/issues/10216))
* Allow `needs` to specify a job that can be started immediately ([gitlab#30631](https://gitlab.com/gitlab-org/gitlab/issues/30631))
* Filters for pipelines ([gitlab#15268](https://gitlab.com/gitlab-org/gitlab/issues/15268))
* Recognizing links in job logs ([gitlab#18324](https://gitlab.com/gitlab-org/gitlab/issues/18324))
* Visualizing job duration ([gitlab#2666](https://gitlab.com/gitlab-org/gitlab/issues/2666))
* Support `.gitlab-ci.yml` outside of repository ([gitlab#14376](https://gitlab.com/gitlab-org/gitlab/issues/14376))

## Top Vision Item(s)

Our most important vision items are our follow ups for the Directed acyclic graphs (DAG) for pipelines MVC [gitlab-org#1716](https://gitlab.com/groups/gitlab-org/-/epics/1716), which will allow for out of order execution and open a world of new possibilities around how pipelines are constructed.

[Cross-project triggered-by registry image change](https://gitlab.com/gitlab-org/gitlab/issues/10074) is also an interesting one for our vision, which would add the ability to depend on container image changes in another project instead of pipelines. This could be a nicer, container-first way of working.
