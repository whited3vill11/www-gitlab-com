---
layout: markdown_page
title: Scheduling Live Upgrade Assistance
description: "Live Upgrade Assitance FAQ"
---


The GitLab support team is here to help. As a part of our [Priority Support](index.html#priority-support) we offer Live Upgrade Assistance. That is, we'll host a live screen share session to help you through the process and ensure there aren't any surprises.

## On This Page
{:.no_toc}

- TOC
{:toc}

## How do I schedule Live Upgrade Assistance?
At least **one week** before your maintainence window, please reach out to your [Technical Account Manager](/handbook/customer-success/tam/#what-is-a-technical-account-manager-tam) with:
   - an upgrade plan
   - an updated archictecture doc
   - any additional relevant information (e.g. *We've had no issues simulating this upgrade in our staging environment*)

If you don't have a Technical Account Manager, but still have Priority Support - please [contact support](https://about.gitlab.com/support/#contact-support) directly to schedule.

**Note**: Weekend upgrades require **two weeks** notice to ensure Engineer availability.

## What does Live Upgrade Assistance Look like?
A GitLab Engineer will join you for the first 30 minutes of your upgrade, to help kick things off and ensure that
you're set up for success by:
- enusring that there's an open ticket for the upgrade that they are monitoring
- going over the upgrade plan once more
- verifying that there is a roll-back plan in place should things not go according to plan.

At that point, the engineer will drop off the call and watch the ticket for any updates for the duration
of the upgrade. If required, they'll rejoin the ongoing call to troubleshoot any issues.

Once the upgrade is complete, and has passed your post-upgrade success criteria, please be sure to send an
update to the ticket that was opened so the Engineer knows they can go offline.

If there haven't been any updates for some time, the Engineer assisting may rejoin the call or send an update to the
ticket requesting an update.

## What versions of GitLab will you support upgrading between?
As noted in our Statement of Support, we [support the current major version and two previous major versions](statement-of-support.html#we-support-the-current-major-version-and-the-two-previous-major-versions). If you're upgrading from a version of GitLab that is no longer supported, Live Upgrade Assistance may not be an option. If you're in this situation, please discuss options with your Technical Account Manager or your Account Manager for Professional Services options.


## What if I don't give a week notice? Will I still be supported?
As a part of [Priority Support](index.html#priority-support), you're also entitled to **24x7 uptime Support**. If
you encounter any issues that are leading to downtime during your upgrade, you can page the on-call engineer to help troubleshoot.

Please provide as much context as you can, including an upgrade plan when you open your emergency ticket.

Do note, that in some cases the best option available may be to invoke your roll-back plan and reschedule the upgrade.
